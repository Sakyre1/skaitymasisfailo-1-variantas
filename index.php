<?php

// DIRECTORY_SEPARATOR adds a slash to the end of the path
define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);
// Set a constant that holds the project's "application" folder, like "/var/www/application".
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);

// Load pre-defined values.
require_once APP . 'config/config.php';

// This is the auto-loader for Composer-dependencies (to load tools into your project).
require_once 'bootstrap.php';

//Start application / Load application class
$app = new Application();