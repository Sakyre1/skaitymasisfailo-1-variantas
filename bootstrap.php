<?php

/**
 * Auto loader based on based on using an anonymous function
 * 
 * @param string $className To define a class that should be loaded.
 *
 * @return void
 */

spl_autoload_register(function($className) {
    // Set pre-defined directories to be use for auto loader
    /** @var \ArrayObject $array An array of pre-defined folders in 'app' folder to be searched in*/
    $dirs = [
        APP . 'Core',
        APP . 'Model',
        APP . 'Model/Read',
        APP . 'Controller',
        APP . 'View',
        APP . 'View/Home'
    ];
    
    // Search a class in every directory
    foreach ($dirs as $dir) {
        // If file (classs) existst in the directory
        if (file_exists($dir . '/' . $className . '.php')) {
            // Class file exists, include and return
            include_once ($dir . '/' . $className . '.php');
            return;
        }
    } 
});


