<?php

class AppController {
    public function index(){
        //Load views:
        //Call Header file
        require_once APP . 'View/_templates/header.php';
        //Call Body file
        require_once APP . 'View/Home/index.php';
        //Call Footer file
        require_once APP . 'View/_templates/footer.php';
    }

    /**
     * ACTION: read
     * This method handles what happens when you move to http://yourproject/home/read
     * 
     */
     public function read()
     {
         // Check if a file is uploaded
         if (isset($_POST["upload"])) {
            $filename = $_FILES["fileUpload"]['name'];
            // Get the extension of file (.csv, .json, .xml)
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            switch ($ext) {
                case 'csv':
                    $rdfrmt = new ReadCsv();
                    break;
                case 'json':
                    $rdfrmt = new ReadJson();
                    break;
                case 'xml':
                    $rdfrmt = new ReadXml();
                    break;
                default:
                    echo 'Something went wrong';
                    break;
            }
            // If file extension found then select class according to file extension
            $Read = new Read($rdfrmt);
            // Ouput to screen data from file
            $Read->readFromFile($_FILES["fileUpload"]);
         }
     }
}