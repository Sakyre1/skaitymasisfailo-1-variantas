<?php

class ErrorController
{
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public function index()
    {
        // Load views:
        require APP . 'View/_templates/header.php';
        require APP . 'View/error/index.php';
        require APP . 'View/_templates/footer.php';
    }
}
